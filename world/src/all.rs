#[derive(Copy, Clone)]
pub enum ForestKind {
    Palm,
    Savannah,
    Oak,
    Pine,
    SnowPine,
}
